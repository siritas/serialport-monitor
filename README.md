When starting Java, use the -Djava.library.path= command line argument to state where the JNI libraries are located. 
For example, you could use  java -Djava.library.path=C:\rxtx-2.2pre2-bins\win32 SerialTest You should also ensure that the RXTXcomm.jar is in your CLASSPATH.
