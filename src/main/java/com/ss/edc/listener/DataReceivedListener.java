package com.ss.edc.listener;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataReceivedListener implements SerialPortPacketListener {

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        byte[] newData = event.getReceivedData();
        log.info("new data length: {}", newData.length);
        for (int i = 0; i < newData.length; i++) {
            log.info("{}", (char) newData[i]);
        }
    }

    @Override
    public int getPacketSize() {
        return 1000;
    }
}
