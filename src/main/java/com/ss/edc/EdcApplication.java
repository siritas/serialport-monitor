package com.ss.edc;

import com.ss.edc.sample.ComProxy;
import com.ss.edc.sample.CommService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class EdcApplication {

    @Bean
    public CommService commService() {
        return new CommService();
    }

    @Bean
    public Ecr ecr() {
        return new Ecr();
    }
    @Bean
    public ComProxy comProxy(){
        return new ComProxy();
    }
    public static void main(String[] args) {
        SpringApplication.run(EdcApplication.class, args);
    }

}
