package com.ss.edc.sample;
/* simple Java serial port proxy logger */
//leverages com0com http://sourceforge.net/projects/com0com/
//and rxtx http://users.frii.com/jarvi/rxtx/download.html

//note, if you installed arduino, then you already have the rxtx jars and shared libraries in your arduino directory
//and if you got avrstudio working with your usbtinyisp, then you have com0com installed too.

//It's a CPU hog so don't leave it running all day.

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

@Slf4j
@Component
public class ComProxy implements /*CommandLineRunner,*/SerialPortEventListener {
    public static final String applicationComPort = "COM5";
    public static final String deviceComport = "COM3";
    public static final int baud = 9600;//19200;


    static InputStream input1;
    static OutputStream output1;

    static InputStream deviceInputStream;
    static OutputStream deviceOutputStream;
    static InputStreamReader inputStreamReader;
    static BufferedReader bufferedReader;
    static OutputStream out;

    //    public static void main(String[] args) throws Exception {
    public void main() throws Exception {
//            out=new FileOutputStream("/comproxy.log");
        out = System.out;

        CommPortIdentifier portId1 = CommPortIdentifier
                .getPortIdentifier(applicationComPort);

        CommPortIdentifier portId2 = CommPortIdentifier
                .getPortIdentifier(deviceComport);

        log.info("Serial Proxy Starting");
        log.info("Serial application port: " + portId1.getName());
        log.info("Serial proxied to device port: " + portId2.getName());


        SerialPort port1 = (SerialPort) portId1.open("serial madness1", 4000);
        input1 = port1.getInputStream();
        output1 = port1.getOutputStream();

        bufferedReader = new BufferedReader(new InputStreamReader(input1));

        port1.setSerialPortParams(baud,
                SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);
        //this assumes the application starts the serial conversation
        while (input1.available() > 0) {
            input1.read();
        }

        log.info("waiting for " + applicationComPort + " activity...");
        while (input1.available() == 0) ;

//        port1.addEventListener(this);
//        port1.notifyOnDataAvailable(true);

        SerialPort devicePort = (SerialPort) portId2.open("serial madness2", 4001);
        deviceInputStream = devicePort.getInputStream();
        deviceOutputStream = devicePort.getOutputStream();

        devicePort.setSerialPortParams(baud,
                SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);
        while (deviceInputStream.available() > 0) {
            deviceInputStream.read();
        }

        log.info("proxy started");

        while (true) {
            int c;
            if (input1.available() > 0) {
                c = input1.read();
                out.write("I".getBytes());
                out.write(hexval(c).getBytes());
                deviceOutputStream.write(c);
            }
            if (deviceInputStream.available() > 0) {
                c = deviceInputStream.read();
                out.write("O".getBytes());
                out.write(hexval(c).getBytes());
                output1.write(c);
            }
        }
    }

    public static void waitfor(int w) throws Exception {
        int c;
        do {
            while (deviceInputStream.available() == 0) ;
            c = deviceInputStream.read();
            System.out.println((char) c + " " + (int) c);
        } while (c != w);
    }


    static String hexvals[] = {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

    static String hexval(int i) {
        return hexvals[i / 16] + hexvals[i % 16];
    }

//    @Override
//    public void run(String... args) throws Exception {
//        ComProxy com = new ComProxy();
//        com.main();
//
//        log.info("start monitoring...");
//    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        log.info("serialEvent()");
        if (serialPortEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            log.info("DATA_AVAILABLE");
//            try {
//                String inputLine = bufferedReader.readLine();
////                log.info(inputLine);
//                System.out.println(inputLine);
//            } catch (Exception e) {
//                log.error(e.getMessage());
////                System.err.println(e.toString());
//            }
        }
    }
}