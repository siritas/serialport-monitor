package com.ss.edc.sample;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class CommService /*implements CommandLineRunner*/ {

    private ThreadPoolExecutor tpe = new ThreadPoolExecutor(0, 5, 600L, TimeUnit.SECONDS, new SynchronousQueue<>());

    /* Default COMM Name under macOS */
//    public static final String DEFAULT_COMM_NAME = "UC-232A";
    public static final String DEFAULT_COMM_NAME = "COM3";
    private SerialPort instance;
    private Timer timer;

    /* get SerialPort instance with default name, COM3 */
    public SerialPort getInstance() throws IllegalAccessException {
        return getInstance(DEFAULT_COMM_NAME);
    }

    public SerialPort getInstance(String commName) throws IllegalAccessException {
        if (null == instance) {
            newInstance(commName);
        }
        return instance;
    }

    private void newInstance(String commName) throws IllegalAccessException {
        log.info("get SerialPort instance {}", commName);

        instance = SerialPort.getCommPort(commName);
        if (!instance.isOpen()) {
            throw new IllegalAccessException("Port not open");
        }
//        instance = SerialPort.getCommPorts()[0];
        instance.setBaudRate(9600);
        instance.setParity(SerialPort.NO_PARITY);
        instance.setNumStopBits(1);
        instance.setNumDataBits(8);

//        instance.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 0, 0);
    }

    public static void listCOMPorts() {
        int idx = 0;
        SerialPort[] comPort = SerialPort.getCommPorts();
        for (SerialPort port : comPort) {
            log.info("sys pn: [{}] {}", idx, port.getSystemPortName());
        }
    }

    SerialPortDataListener dataAvail = new SerialPortDataListener() {
        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
        }

        @Override
        public void serialEvent(SerialPortEvent event) {
            log.info("event {}", event.getEventType());

        }
    };
    SerialPortDataListener dataWritten = new SerialPortDataListener() {
        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_WRITTEN;
        }

        @Override
        public void serialEvent(SerialPortEvent event) {
            log.info("event {}", event.getEventType());

        }
    };

    public void listenCOMPorts() {

        if (null == instance) {
            throw new IllegalStateException("user should call getInstance() first");
        }

        log.info("listenComPorts() {}", instance.getSystemPortName());
        instance.openPort();
//        instance.addDataListener();
        TimerTask everyXSec = new TimerTask() {

            @Override
            public void run() {
                log.info("waiting COMM events...");
            }
        };
        timer = new Timer("WaitingMessage");
//        timer.scheduleAtFixedRate(everyXSec, TimeUnit.SECONDS.toMillis(5), TimeUnit.SECONDS.toMillis(5));

    }

    public void closeCOM() {
        if (null != instance) {
            log.info("closing port ");
//            timer.cancel();
            instance.removeDataListener();
            instance.closePort();
        }
    }


//    @Override
//    public void run(String... args) throws Exception {

//        CommService.listCOMPorts();
//        CommService.getInstance();
//
//        this.listenCOMPorts();
//        Thread.sleep(TimeUnit.MINUTES.toMillis(1));
//        this.closeCOM();
//    }
}

// 02 00 35 36 30 30 30 30 30 30 30 30 30 31 30 32 30 30 30 30 1C 34 30 00 12 30 30 30 30 30 30 30 30 33 39 30 30 1C 03 1F