package com.ss.edc.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;

@Slf4j
public class SerialPortUtils {

    private static Hex hex = new Hex(Charsets.toCharset("US-ASCII"));

    /*
    *  from: public byte[] PackSerial(byte[] byData, int inDataLen)
    *
    *  format serial = STX+LL+LL+DATA+ETX+LRC
    * **/
    public static byte[] byteToSerialPort(byte[] data) throws EncoderException, UnsupportedEncodingException, DecoderException {
        log.debug("byteToSerialPort()");

        int dataLength = data.length;
        byte[] result = new byte[dataLength + 5];

        String lengthField = String.format("%04d", dataLength);
        byte[] byteLength = Hex.decodeHex(lengthField.toCharArray());
        //hex.encode(lengthField.getBytes("US-ASCII"));
        log.debug(" length byte: {}", byteLength);

        result[0] = 0x02;
        result[1] = byteLength[0];
        result[2] = byteLength[1];

        System.arraycopy(data, 0, result, 3, dataLength);
        //ใส่ 0x03 ที่ตำแหน่ง data length + 3. i.e.: data length: 41 จะใส่ที่ position 44
        result[dataLength + 3] = 0x03;

        byte byteLRC = getLRC(result, 1, dataLength + 3);
        //ใส่  LRC ที่ตำแหน่ง data length + 4. i.e.: data length: 41 จะใส่ที่ position 44
        result[dataLength + 4] = byteLRC;

        return result;
    }

    //    [a longitudinal redundancy check](https://gist.github.com/neuro-sys/953550)
    private static byte getLRC(byte[] data, int offset, int length) {

        byte LRC = 0;
        for (int i = offset; i < data.length; i++) {
            LRC ^= data[i];
        }
        log.debug("lrc:{}", LRC);
        return LRC;
    }

    private static byte[] asciiToHex(String hex) {
        byte[] result = new byte[hex.length() / 2];

        int hexIndex = 0;
        for (int i = 0; i < result.length; i++) {
            result[i] = (byte) (
                    parseHexDigit(hex.charAt(hexIndex++)) * 16
                            + parseHexDigit(hex.charAt(hexIndex++))
            );
        }
        return result;
    }

    private static byte[] asciiToHex2(String hex) throws UnsupportedEncodingException, DecoderException {
//        byte[] result = new byte[hex.length() / 2];
//
//        int hexIndex = 0;
//        for (int i = 0; i < result.length; i++) {
//            result[i] = (byte) (
//                    parseHexDigit(hex.charAt(hexIndex++)) * 16
//                            + parseHexDigit(hex.charAt(hexIndex++))
//            );
//        }
//        char[] chars = hex.toCharArray();
//        StringBuilder sb = new StringBuilder();
//        for (char c : chars) {
//            sb.append(Integer.toHexString((int) c));
//        }
        return Hex.decodeHex(hex.toCharArray());
    }

    private static int parseHexDigit(char cc) {
        log.debug("parseHexDigit() {}", cc);
        int c = (int) cc;
        log.debug("int value {}", c);
        if (c >= 0x30 && c <= 0x93) {
            return c - 0x30;
        }
        if (c >= 0x41 && c <= 0x46) {
            return c - 0x41 + 10;
        }
        if (c >= 0x61 && c <= 0x66) {
            return c - 0x66 + 10;
        }
        return c;
    }
}
