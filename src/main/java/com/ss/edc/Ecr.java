package com.ss.edc;

import com.ss.edc.util.SerialPortUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

@Slf4j
public class Ecr {

    private final Charset CHARSET_ASCII = Charset.forName("US-ASCII");

    // PackECRSale()
    public byte[] prepareSaleData(String amount, String merchantId) throws EncoderException, UnsupportedEncodingException, DecoderException {

        log.debug("prepareSaleData({}, {}) ", amount, merchantId);

        if (!org.apache.commons.lang.StringUtils.isNumeric(amount)) {
            throw new IllegalArgumentException("Amount should be the number only");
        }

        byte[] dataToSend = {
                0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
                0x31, 0x30, 0x32, 0x30, 0x30, 0x30, 0x30, 0x1C,
                // Amount
                0x34, 0x30, 0x00, 0x12, 0x30, 0x30, 0x30, 0x30, 0x30,
                0x30, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1C,
                // Merchant no.
                0x34, 0x35, 0x00, 0x01, 0x30, 0x1C
        };

        String amountWidth12 = String.format("%012d", Long.parseLong(amount));
        log.debug("amount str:{}", amountWidth12);
        byte[] amountASCIIBytes = amountWidth12.getBytes(CHARSET_ASCII);
        System.arraycopy(amountASCIIBytes, 0, dataToSend, 22, 12);

        String merchantWidth1 = String.format("%1d", Long.parseLong(merchantId));
        log.debug("merchant str:{}", merchantWidth1);
        byte[] merchantASCIIBytes = merchantWidth1.getBytes(CHARSET_ASCII);
        System.arraycopy(merchantASCIIBytes, 0, dataToSend, 39, 1);

        return SerialPortUtils.byteToSerialPort(dataToSend);
    }
}
