package com.ss.edc.service;


import com.ss.edc.Ecr;
import com.ss.edc.sample.ComProxy;
import com.ss.edc.sample.CommService;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

@Slf4j
@Component
public class EcrService implements CommandLineRunner{


    @Autowired
    CommService commService;

    @Autowired
    Ecr ecrInstance;

    @Autowired
    ComProxy comProxy;


    private void execute() throws EncoderException, DecoderException, UnsupportedEncodingException, IllegalAccessException {
        //        Ecr ecrInstance = new Ecr();
        byte[] toEDC = ecrInstance.prepareSaleData("15000", "1");

        log.debug("toEDC {}", Hex.encodeHexString(toEDC));
/*
        CommService.listCOMPorts();

        SerialPort serialPort = commService.getInstance();
        serialPort.openPort();

        //Listen data written
        serialPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_WRITTEN;
            }

            @Override
            public void serialEvent(com.fazecast.jSerialComm.SerialPortEvent event) {
                log.info("event {}", event.getEventType());
                if (SerialPort.LISTENING_EVENT_DATA_WRITTEN == event.getEventType()) {
                    log.info("All bytes successfully transmitted!");
                }
            }
        });
        //Listen data read
        serialPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            @Override
            public void serialEvent(com.fazecast.jSerialComm.SerialPortEvent event) {
                log.info("event {}", event.getEventType());
                if (event.getEventType() == SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
                    byte[] newData = new byte[serialPort.bytesAvailable()];
                    int numRead = serialPort.readBytes(newData, newData.length);
                    log.info("Read " + numRead + " bytes.");
                }
            }
        });

        serialPort.writeBytes(toEDC, toEDC.length);

        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        serialPort.removeDataListener();
        serialPort.closePort();
        */
    }

    @Override
    public void run(String... args) throws Exception {
//        comProxy.main();
        log.info("EcrService.run() ");
        try {
            execute();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
